/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useEffect} from 'react';
import {StatusBar} from 'react-native';
import {persistStore} from 'redux-persist';
import {Provider} from 'react-redux';
import {store} from './src/redux/store';
import {NavigationContainer} from '@react-navigation/native';
import SplashScreen from 'react-native-splash-screen';
import Toast from 'react-native-toast-message';
import * as RNLocalize from 'react-native-localize';
import i18n from 'i18n-js';
import memoize from 'lodash.memoize';
import {withIAPContext} from 'react-native-iap';
import {NotifierWrapper} from 'react-native-notifier';

import MainNavigator from './src/navigation/MainNavigator';
import {PersistGate} from 'redux-persist/integration/react';

const translationGetters = {
  en: () => require('./src/translations/en.json'),
  fr: () => require('./src/translations/fr.json'),
};

export const translate = memoize(
  (key, config) => i18n.t(key, config),
  (key, config) => (config ? key + JSON.stringify(config) : key),
);

const setI18nConfig = () => {
  const fallback = {languageTag: 'en'};
  const {languageTag} =
    RNLocalize.findBestAvailableLanguage(Object.keys(translationGetters)) ||
    fallback;

  translate.cache.clear();

  i18n.translations = {[languageTag]: translationGetters[languageTag]()};
  i18n.locale = languageTag;
};

const App = () => {
  const config = {
    screens: {
      Settings: 'settings',
    },
  };

  const linking = {
    prefixes: ['shufflapp://'],
    config,
  };

  useEffect(() => {
    SplashScreen.hide();
  }, []);

  useEffect(() => {
    setI18nConfig();
  }, []);

  const handleLocalizationChange = () => {
    setI18nConfig()
      .then(() => this.forceUpdate())
      .catch(error => {
        console.error(error);
      });
  };

  useEffect(() => {
    RNLocalize.addEventListener('change', handleLocalizationChange);

    return () => {
      RNLocalize.removeEventListener('change', handleLocalizationChange);
    };
  }, []);

  let persistor = persistStore(store);

  return (
    <>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <NotifierWrapper>
            <NavigationContainer linking={linking}>
              <StatusBar barStyle="light-content" />
              <MainNavigator />
            </NavigationContainer>
          </NotifierWrapper>
        </PersistGate>
      </Provider>
      <Toast />
    </>
  );
};

export default withIAPContext(App);
