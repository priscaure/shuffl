import React, {useEffect, useState, useCallback} from 'react';
import {Platform} from 'react-native';
import RNIap, {requestSubscription, useIAP} from 'react-native-iap';
import {useDispatch, useSelector} from 'react-redux';
import {
  APPLE_RECEIPT_PSW,
  FIREBASE_LINK_IOS,
  FIREBASE_LINK_ANDROID,
} from '@env';

import Loading from '../components/Loading';
import {
  selectUserPremium,
  setExpired,
  setPremium,
  setProducts,
} from '../redux/premiumSlice';
import {translate} from '../../App';
import {setSwiperIndex} from '../redux/swiperSlice';
import AsyncStorage from '@react-native-async-storage/async-storage';

const items = Platform.select({
  ios: ['com.priscaure.shufflapp.abo.30days'],
  android: ['com.priscaure.shufflapp.abo.30days'],
});

const useInAppPurchase = () => {
  const [loading, setLoading] = useState(false);
  const [connectionErrorMsg, setConnectionErrorMsg] = useState('');
  const {expired} = useSelector(selectUserPremium);
  const dispatch = useDispatch();

  const validate = useCallback(
    async receipt => {
      const receiptBodyIOS = {
        'receipt-data': receipt,
        password: APPLE_RECEIPT_PSW,
      };

      if (Platform.OS === 'ios') {
        const deliveryReceipt = await fetch(FIREBASE_LINK_IOS, {
          headers: {
            'Content-Type': 'application/json',
          },
          method: 'POST',
          body: JSON.stringify({
            data: receiptBodyIOS,
          }),
        })
          .then(res =>
            res
              .json()
              .then(r => {
                if (r.result.isExpired) {
                  dispatch(setExpired(true));
                  dispatch(setPremium(false));
                } else {
                  dispatch(setPremium(true));
                  dispatch(setExpired(false));
                  dispatch(setSwiperIndex(0));
                  AsyncStorage.removeItem('@lastSwipe');
                }
              })
              .catch(err => {
                console.log('ERR: ', err);
              }),
          )
          .catch(err => {
            console.log('ERR: ', err);
          });
      }
      if (Platform.OS === 'android') {
        const deliveryReceipt = await fetch(FIREBASE_LINK_ANDROID, {
          headers: {
            'Content-Type': 'application/json',
          },
          method: 'POST',
          body: JSON.stringify({
            data: receipt,
          }),
        })
          .then(res =>
            res
              .json()
              .then(r => {
                if (r.result.error === -1) {
                  dispatch(setExpired(true));
                  dispatch(setPremium(false));
                } else if (r.result.isActiveSubscription) {
                  dispatch(setPremium(true));
                  dispatch(setExpired(false));
                  dispatch(setSwiperIndex(0));
                  AsyncStorage.removeItem('@lastSwipe');
                } else {
                  dispatch(setExpired(true));
                  dispatch(setPremium(false));
                }
              })
              .catch(err => {
                console.log('ERR: ', err);
              }),
          )
          .catch(err => {
            console.log('ERR: ', err);
          });
      }
    },
    [dispatch],
  );

  const {
    connected,
    subscriptions,
    getSubscriptions,
    finishTransaction,
    currentPurchase,
    currentPurchaseError,
  } = useIAP();

  useEffect(() => {
    RNIap.getPurchaseHistory()
      .catch(() => {})
      .then(res => {
        const receipt = res[res?.length - 1].transactionReceipt;
        if (receipt) {
          validate(receipt);
        }
      });
  }, [validate]);

  // Get products from play store.
  useEffect(() => {
    if (connected) {
      setLoading(true);
      getSubscriptions(items);
      setLoading(false);
    }
  }, [connected, getSubscriptions]);

  // currentPurchase will change when the requestPurchase function is called. The purchase then needs to be checked and the purchase acknowledged so Google knows we have awared the user the in-app product.
  useEffect(() => {
    const checkCurrentPurchase = async purchase => {
      if (purchase) {
        const receipt = purchase.transactionReceipt;
        if (receipt) {
          validate(receipt);
          try {
            const ackResult = await finishTransaction(purchase, false);
            console.log('ackResult: ', ackResult);
          } catch (ackErr) {
            console.log('ackError: ', ackErr);
          }
        }
      }
    };
    checkCurrentPurchase(currentPurchase);
  }, [currentPurchase, finishTransaction, validate]);

  useEffect(() => {
    if (currentPurchaseError) {
      if (currentPurchaseError.code === 'E_ALREADY_OWNED' && !expired) {
        dispatch(setPremium(true));
        dispatch(setExpired(false));
        dispatch(setSwiperIndex(0));
      }
    }
  }, [currentPurchaseError, dispatch, expired]);

  const purchasePremium = async () => {
    // Reset error msg
    if (connectionErrorMsg !== '') {
      setConnectionErrorMsg('');
    }
    if (!connected) {
      setConnectionErrorMsg(translate('connexion'));
    }
    // If we are connected & have products, purchase the item. Gohas no inteogle will handle if user rnet here.
    else if (subscriptions?.length > 0) {
      console.log('items: ', items);
      requestSubscription(items[0]);
      console.log('Purchasing products...');
    }
    // If we are connected but have no products returned, try to get products and purchase.
    else {
      try {
        const storeProducts = await getSubscriptions(items);
        if (storeProducts.length > 0) {
          dispatch(setProducts(storeProducts));
        }
        requestSubscription(items[0]);
        console.log('Got products, now purchasing...');
      } catch (error) {
        setConnectionErrorMsg(translate('connexion'));
        console.log('Everything failed. Error: ', error);
      }
    }
  };

  if (loading) {
    return <Loading />;
  }

  return {
    connectionErrorMsg,
    purchasePremium,
  };
};

export default useInAppPurchase;
