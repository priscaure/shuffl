export const theme = {
  color: {
    background: '#11111E',
    lighter: '#23263E',
    primary: '#9D5CFF',
    secondary: '#02D9DF',
    text: '#CFCFFB',
    black: '#000000',
    border: '#373C62',
    spotify: '#1ED760',
  },
  text: {
    size: {
      sm: 14,
      base: 16,
      md: 18,
      lg: 20,
      xl: 24,
      xxl: 36,
    },
  },
  space: {
    xxs: 4,
    xs: 8,
    sm: 16,
    md: 24,
    lg: 36,
    xl: 40,
    xxl: 48,
  },
  radius: {
    base: 16,
    lg: 20,
    xl: 30,
  },
};
