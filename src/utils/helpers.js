import {refresh} from 'react-native-app-auth';
import {config} from '../api/spotify';
import {SPOTIFY_API_URL, CLIENT_SECRET, CLIENT_ID} from '@env';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const refreshLogin = async refreshToken => {
  const result = await refresh(config, {
    refreshToken: refreshToken,
  });
  console.log('RES/ ', result);
  return result;
};

export const getUserId = async token => {
  const userData = await fetch(`${SPOTIFY_API_URL}/me`, {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + token,
    },
  })
    .then(response => response.json())
    .then(res => {
      return res;
    })
    .catch(error => {
      console.log('error', error);
    });

  return userData;
};

export const refreshToken = async token => {
  const hasRefreshToken = await AsyncStorage.getItem('@refreshToken');
  const parsedRefreshToken = JSON.parse(hasRefreshToken);
  const myHeaders = new Headers();
  myHeaders.append('Content-Type', 'application/x-www-form-urlencoded');

  const requestOptions = {
    method: 'POST',
    headers: myHeaders,
    body: `grant_type=refresh_token&refresh_token=${parsedRefreshToken}&client_id=${CLIENT_ID}&client_secret=${CLIENT_SECRET}`,
  };

  const refreshTokenData = await fetch(
    'https://accounts.spotify.com/api/token',
    requestOptions,
  )
    .then(response => response.json())
    .then(res => {
      return res;
    })
    .catch(error => {
      console.log('error', error);
    });

  return refreshTokenData;
};

export const fetchArtists = async token => {
  const artistsData = await fetch(`${SPOTIFY_API_URL}/me/top/artists?limit=5`, {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + token,
    },
  })
    .then(res => res.json())
    .then(artists => {
      return artists;
    })
    .catch(err => console.log(err));

  return artistsData;
};

export const fetchFeaturedPlaylists = async token => {
  const fetchFeaturedPlaylistsData = await fetch(
    `${SPOTIFY_API_URL}/browse/featured-playlists?limit=1`,
    {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
      },
    },
  )
    .then(res => res.json())
    .then(playlistsRes => {
      return playlistsRes?.playlists?.items[0].id;
    })
    .catch(err => console.log(err));
  return fetchFeaturedPlaylistsData;
};

export const fetchPlaylist = async data => {
  const fetchPlaylistsData = await fetch(
    `${SPOTIFY_API_URL}/playlists/${data.id}`,
    {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + data.token,
      },
    },
  )
    .then(res => res.json())
    .then(playlistTracks => {
      return playlistTracks;
    })
    .catch(err => console.log(err));
  return fetchPlaylistsData;
};

export const fetchReleases = async token => {
  const fetchTracksData = await fetch(
    `${SPOTIFY_API_URL}/browse/new-releases?limit=50`,
    {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
      },
    },
  )
    .then(res => res.json())
    .then(releases => {
      return releases?.albums?.items;
    })
    .catch(err => console.log(err));
  return fetchTracksData;
};

export const fetchTracks = async token => {
  const fetchTracksData = await fetch(
    `${SPOTIFY_API_URL}/me/top/tracks?limit=5`,
    {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
      },
    },
  )
    .then(res => res.json())
    .then(topTracks => {
      return topTracks;
    })
    .catch(err => console.log(err));
  return fetchTracksData;
};

export const fetchRecos = async ({seedArtists, seedtracks, limit, token}) => {
  const fetchRecosData = fetch(
    `${SPOTIFY_API_URL}/recommendations?seed_artists=${seedArtists}&seed_tracks=${seedtracks}&limit=${limit}`,
    {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + token,
      },
    },
  )
    .then(res => res.json())
    .then(recoRes => {
      console.log(recoRes.tracks);
      return recoRes.tracks;
    })
    .catch(err => console.log(err));
  return fetchRecosData;
};

export const fetchDevices = async ({token}) => {
  const fetchDevicesData = await fetch(`${SPOTIFY_API_URL}/me/player/devices`, {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: 'Bearer ' + token,
    },
  })
    .then(res => res.json())
    .then(userDevice => {
      return userDevice;
    })
    .catch(err => console.log(err));

  return fetchDevicesData;
};
