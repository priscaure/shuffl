import {Linking} from 'react-native';

class AppInstalledChecker {
  static checkURLScheme(proto, query) {
    return new Promise((resolve, reject) => {
      Linking.canOpenURL(proto + '://' + query || '')
        .then(isInstalled => {
          resolve(isInstalled);
        })
        .catch(err => {
          reject(err);
        });
    });
  }
}

export default AppInstalledChecker;
