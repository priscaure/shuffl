import {Text, StyleSheet, Platform} from 'react-native';
import React from 'react';
import {theme} from '../theme/theme';

const SettingsTitle = ({title}) => {
  return <Text style={styles.title}>{title}</Text>;
};

const styles = StyleSheet.create({
  title: {
    color: theme.color.primary,
    fontFamily: 'FuturaStd-Bold',
    fontWeight: 'bold',
    fontSize: theme.text.size.lg,
    marginBottom: theme.space.md,
    letterSpacing: -1,
  },
});

export default SettingsTitle;
