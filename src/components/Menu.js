import {Image, StyleSheet, TouchableOpacity} from 'react-native';
import React from 'react';

import menu from '../assets/menu.png';
import {useNavigation} from '@react-navigation/native';
import {activeOpacity} from '../utils/constants';

const Menu = () => {
  const navigation = useNavigation();

  return (
    <TouchableOpacity
      activeOpacity={activeOpacity}
      onPress={() => navigation.navigate('Settings')}>
      <Image source={menu} />
    </TouchableOpacity>
  );
};

export const styles = StyleSheet.create({
  menu: {},
});

export default Menu;
