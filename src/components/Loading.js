import {View, StyleSheet} from 'react-native';
import React from 'react';
import LottieView from 'lottie-react-native';
import {theme} from '../theme/theme';

const Loading = () => {
  return (
    <View style={styles.container}>
      <LottieView
        source={require('../animations/loading.json')}
        autoPlay
        loop
        style={styles.loader}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.color.background,
    justifyContent: 'center',
    alignItems: 'center',
  },
  loader: {
    width: 100,
    height: 100,
  },
});

export default Loading;
