import {View, Text, Image, StyleSheet} from 'react-native';
import React from 'react';
import {theme} from '../theme/theme';
import confetti from '../assets/confetti.png';
import {translate} from '../../App';
import PremiumButton from './Premium/PremiumButton';

const BottomSheetContent = () => {
  return (
    <View style={styles.contentContainer}>
      <Image source={confetti} />
      <Text style={styles.sheetTitle}>{translate('limitedShuffleTitle')}</Text>
      <Text style={styles.sheetText}>{translate('limitedShuffleText')}</Text>
      <PremiumButton />
    </View>
  );
};

const styles = StyleSheet.create({
  contentContainer: {
    flex: 0.85,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: theme.space.xl,
    paddingTop: theme.space.md,
    textAlign: 'center',
  },
  spaceBtn: {
    marginBottom: theme.space.md,
  },
  sheetTitle: {
    fontFamily: 'FuturaStd-Bold',
    color: theme.color.text,
    fontSize: theme.text.size.xl,
    textAlign: 'center',
    lineHeight: 32,
    marginVertical: theme.space.sm,
  },
  sheetText: {
    fontFamily: 'Roboto-Regular',
    fontSize: theme.text.size.base,
    lineHeight: 20,
    textAlign: 'center',
    color: theme.color.text,
    width: '80%',
  },
  sheetBtn: {
    backgroundColor: theme.color.primary,
    height: 56,
    width: '100%',
    borderRadius: 56,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: theme.space.md,
  },
  sheetBtnText: {
    fontFamily: 'Roboto-Bold',
    fontSize: theme.text.size.base,
  },
});

export default React.memo(BottomSheetContent);
