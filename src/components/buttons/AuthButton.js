import React from 'react';
import {Text, TouchableOpacity, StyleSheet, View, Image} from 'react-native';
import SpotifyIcon from 'react-native-vector-icons/FontAwesome';
import {useDispatch} from 'react-redux';
import {Notifier, NotifierComponents, Easing} from 'react-native-notifier';

import {theme} from '../../theme/theme';
import spotifyIcon from '../../assets/spotifyIcon.png';
import {translate} from '../../../App';
import {authorize} from 'react-native-app-auth';
import {config} from '../../api/spotify';
import {setToken} from '../../redux/userSlice';
import {activeOpacity} from '../../utils/constants';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {getUserId} from '../../utils/helpers';
import OneSignal from 'react-native-onesignal';

const AuthButton = ({text}) => {
  const dispatch = useDispatch();

  const onLogin = async () => {
    try {
      const result = await authorize(config);
      console.log(JSON.stringify(result));
      const user = await getUserId(result.accessToken);
      let externalUserId = user.id;
      OneSignal.setExternalUserId(externalUserId);
      AsyncStorage.setItem(
        '@refreshToken',
        JSON.stringify(result.refreshToken),
      );
      AsyncStorage.setItem(
        '@tokenExp',
        JSON.stringify(
          new Date(result.accessTokenExpirationDate).getTime() / 1000,
        ),
      );
      dispatch(setToken(result.accessToken));
      return result;
    } catch (error) {
      console.log('LOGIN ERROR: ', JSON.stringify(error));
      Notifier.showNotification({
        title: `${error.code} 😱`,
        description: translate('loginError'),
        Component: NotifierComponents.Alert,
        componentProps: {
          alertType: 'error',
        },
        duration: 5000,
        showAnimationDuration: 800,
        showEasing: Easing.ease,
        hideOnPress: true,
      });
    }
  };

  return (
    <TouchableOpacity
      activeOpacity={activeOpacity}
      style={styles.button}
      onPress={onLogin}>
      <View style={styles.textWrap}>
        <Text style={styles.text}>{text}</Text>
        <Image source={spotifyIcon} style={styles.spotifyIcon} />
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    height: 64,
    backgroundColor: theme.color.black,
    borderRadius: theme.radius.xl,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: theme.space.xxl,
  },
  text: {
    color: theme.color.text,
    fontSize: theme.text.size.md,
    marginRight: theme.space.sm,
    fontFamily: 'Roboto-Bold',
  },
  textWrap: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  spotifyIcon: {
    width: 28,
    height: 28,
    marginRight: theme.space.xs,
  },
});

export default React.memo(AuthButton);
