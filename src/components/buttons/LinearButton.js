import {Text, TouchableOpacity, StyleSheet} from 'react-native';
import React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import {activeOpacity} from '../../utils/constants';
import {theme} from '../../theme/theme';

const LinearButton = ({title, disabled}) => {
  return (
    <TouchableOpacity activeOpacity={activeOpacity} style={styles.linearWrap}>
      <LinearGradient
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
        colors={[theme.color.primary, theme.color.secondary]}
        style={styles.premBtn}>
        <Text style={styles.premBtnText}>{title}</Text>
      </LinearGradient>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  linearWrap: {
    width: '100%',
  },
  premBtn: {
    height: 56,
    width: '100%',
    borderRadius: 56,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: theme.space.md,
  },
  premBtnText: {
    fontFamily: 'Roboto-Bold',
    fontSize: theme.text.size.base,
  },
});

export default LinearButton;
