import {TouchableOpacity, StyleSheet} from 'react-native';
import React from 'react';
import {useDispatch} from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-async-storage/async-storage';

import {activeOpacity} from '../../utils/constants';
import {theme} from '../../theme/theme';
import {setToken} from '../../redux/userSlice';

const LogoutButton = () => {
  const dispatch = useDispatch();
  const onLogout = async () => {
    AsyncStorage.removeItem('@token');
    AsyncStorage.removeItem('@tokenExp');
    dispatch(setToken(null));
  };
  return (
    <TouchableOpacity
      style={styles.close}
      activeOpacity={activeOpacity}
      onPress={onLogout}>
      <Icon
        name="power"
        color={theme.color.text}
        size={28}
        style={styles.iconClose}
      />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  close: {
    paddingHorizontal: theme.space.md,
    alignItems: 'center',
  },
  iconClose: {
    opacity: 0.3,
  },
});

export default LogoutButton;
