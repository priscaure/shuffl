import {Image, StyleSheet} from 'react-native';
import React from 'react';

import authLogo from '../../assets/authLogo.png';

const AuthLogo = () => {
  return <Image source={authLogo} style={styles.logo} />;
};

const styles = StyleSheet.create({
  logo: {
    marginBottom: 20,
  },
});

export default AuthLogo;
