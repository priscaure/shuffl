import {Text, StyleSheet} from 'react-native';
import React from 'react';
import {theme} from '../../theme/theme';

const AuthText = ({first, second}) => {
  return (
    <>
      <Text style={styles.text}>{first}</Text>
      <Text style={styles.text}>{second}</Text>
    </>
  );
};

const styles = StyleSheet.create({
  text: {
    color: theme.color.text,
    textAlign: 'center',
    fontSize: theme.text.size.md,
    fontFamily: 'FuturaStd-Medium',
    lineHeight: 24,
  },
});

export default AuthText;
