import React from 'react';
import {ImageBackground, StyleSheet} from 'react-native';

import loginBg from '../../assets/loginBg.png';

const AuthBackground = ({children}) => {
  return (
    <ImageBackground source={loginBg} style={styles.background}>
      {children}
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  background: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default AuthBackground;
