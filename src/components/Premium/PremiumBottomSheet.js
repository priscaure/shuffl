import {StyleSheet} from 'react-native';
import React, {useMemo, useCallback, useRef} from 'react';
import BottomSheet from '@gorhom/bottom-sheet';

import BackDropCustom from '../BackDropCustom';
import BottomSheetContent from '../BottomSheetContent';
import {theme} from '../../theme/theme';

const PremiumBottomSheet = () => {
  const bottomSheetRef = useRef(null);
  const snapPoints = useMemo(() => ['25%', '50%'], []);

  const handleSheetChanges = useCallback(sheetIndex => {
    console.log('handleSheetChanges', sheetIndex);
  }, []);

  return (
    <BottomSheet
      backdropComponent={BackDropCustom}
      backgroundStyle={styles.bg}
      ref={bottomSheetRef}
      index={1}
      snapPoints={snapPoints}
      onChange={handleSheetChanges}>
      <BottomSheetContent />
    </BottomSheet>
  );
};

const styles = StyleSheet.create({
  bg: {
    backgroundColor: theme.color.lighter,
    shadowColor: theme.color.black,
    shadowOpacity: 0.5,
    shadowOffset: {width: 0, height: 9},
    shadowRadius: 12.35,
    elevation: 19,
  },
});

export default React.memo(PremiumBottomSheet);
