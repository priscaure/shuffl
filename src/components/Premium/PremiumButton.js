import {Text, TouchableOpacity, StyleSheet} from 'react-native';
import React, {useEffect} from 'react';
import LinearGradient from 'react-native-linear-gradient';
import * as RNIap from 'react-native-iap';

import {activeOpacity} from '../../utils/constants';
import {theme} from '../../theme/theme';
import {translate} from '../../../App';
import {useSelector} from 'react-redux';
import {selectUserPremium} from '../../redux/premiumSlice';
import useInAppPurchase from '../../hooks/useInAppPurchase';

const PremiumButton = () => {
  const {purchasePremium} = useInAppPurchase();
  const {premium, expired} = useSelector(selectUserPremium);

  /* useEffect(() => {
    RNIap.initConnection()
      .catch(err => {
        console.log('Error connecting to store: ', err);
      })
      .then(async () => {
        console.log('connected to store');
      });
  }, []); */

  return (
    <TouchableOpacity
      activeOpacity={activeOpacity}
      style={styles.linearWrap}
      onPress={purchasePremium}
      disabled={premium}>
      <LinearGradient
        start={{x: 0, y: 0}}
        end={{x: 1, y: 0}}
        colors={
          !premium || expired
            ? [theme.color.primary, theme.color.secondary]
            : [theme.color.lighter, theme.color.lighter]
        }
        style={styles.premBtn}>
        <Text style={styles.premBtnText}>
          {!expired ? translate('premium') : translate('expiredPremium')}
        </Text>
      </LinearGradient>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  linearWrap: {
    width: '100%',
  },
  premBtn: {
    height: 56,
    width: '100%',
    borderRadius: 56,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: theme.space.md,
  },
  premBtnText: {
    fontFamily: 'Roboto-Bold',
    fontSize: theme.text.size.base,
  },
});

export default PremiumButton;
