import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/Feather';
import {theme} from '../../theme/theme';
import {activeOpacity} from '../../utils/constants';

const SettingsRow = ({iconName, text, onPress, chevron}) => {
  return (
    <TouchableOpacity
      activeOpacity={activeOpacity}
      style={styles.touchable}
      onPress={onPress}>
      <View style={styles.outerRow}>
        <Icon name={iconName} size={18} color={theme.color.text} />
        <Text style={styles.rowText}>{text}</Text>
      </View>
      {chevron && (
        <Icon name="chevron-right" size={24} color={theme.color.text} />
      )}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  touchable: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: theme.space.xs,
  },
  outerRow: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  rowText: {
    color: theme.color.text,
    fontSize: theme.text.size.base,
    fontFamily: 'Roboto-Regular',
    marginLeft: theme.space.xs,
    lineHeight: 24,
  },
});

export default SettingsRow;
