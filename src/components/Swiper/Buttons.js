import {View, TouchableOpacity, StyleSheet} from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';

import {theme} from '../../theme/theme';
import {activeOpacity} from '../../utils/constants';

const Buttons = ({onPressLeft, onPressRight, disabled}) => {
  return (
    <View style={styles.buttonsWrapper}>
      <TouchableOpacity
        activeOpacity={activeOpacity}
        onPress={onPressLeft}
        disabled={disabled}>
        <View
          style={[
            styles.swipeBtn,
            {
              backgroundColor: disabled
                ? theme.color.lighter
                : theme.color.secondary,
            },
          ]}>
          <Icon name="close" size={24} color={theme.color.background} />
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        activeOpacity={activeOpacity}
        onPress={onPressRight}
        disabled={disabled}>
        <View
          style={[
            styles.swipeBtn,
            {
              backgroundColor: disabled
                ? theme.color.lighter
                : theme.color.primary,
            },
          ]}>
          <Icon name="check" size={24} color={theme.color.background} />
        </View>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  buttonsWrapper: {
    flexDirection: 'row',
    height: 120,
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: theme.space.xxl,
  },
  swipeBtn: {
    borderRadius: 48,
    height: 48,
    width: 48,
    alignItems: 'center',
    justifyContent: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.7,
    shadowRadius: 9,
    elevation: 14,
  },
});

export default Buttons;
