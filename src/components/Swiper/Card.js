import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  Platform,
  Linking,
  Alert,
} from 'react-native';
import React, {useState, useCallback} from 'react';
import FastImage from 'react-native-fast-image';

import {theme} from '../../theme/theme';
import {activeOpacity, HEIGHT, WIDTH} from '../../utils/constants';
import spotifyIcon from '../../assets/spotifyIcon.png';
import AppInstalledChecker from '../../utils/app-installed-checker';

const Card = ({card}) => {
  const [supported, setSupported] = useState(false);
  const cardUrl =
    Platform.OS === 'ios'
      ? card?.uri
        ? card?.uri
        : card?.track?.uri
      : card?.external_urls
      ? card?.external_urls?.spotify
      : card?.track?.external_urls?.spotify;

  AppInstalledChecker.checkURLScheme('spotify').then(isInstalled => {
    setSupported(isInstalled);
  });

  const handleUri = useCallback(async url => {
    const linkSupported = await Linking.canOpenURL(url);

    if (linkSupported) {
      await Linking.openURL(url);
    } else {
      Alert.alert(`Don't know how to open this URL: ${url}`);
    }
  }, []);

  const handleInstall = () => {
    if (Platform.OS === 'ios') {
      Linking.openURL(
        'itms-apps://apps.apple.com/app/spotify-music/id324684580',
      );
    } else {
      Linking.openURL(
        'https://play.google.com/store/apps/details?id=com.spotify.music',
      );
    }
  };

  return (
    <View key={card?.id} style={styles.cardWrapper}>
      <View style={styles.card}>
        <FastImage
          source={{
            uri: card?.album
              ? card?.album?.images[0]?.url
              : card?.track?.album?.images[0]?.url,
          }}
          resizeMode={FastImage.resizeMode.contain}
          style={styles.image}
        />
      </View>
      <View style={styles.textWrapper}>
        <Text style={styles.artist}>
          {card?.artists
            ? card?.artists[0]?.name
            : card?.track?.artists[0]?.name}
        </Text>
        <Text style={styles.song} numberOfLines={1}>
          {card?.name ? card?.name : card?.track?.name}
        </Text>
      </View>
      {supported ? (
        <TouchableOpacity
          activeOpacity={activeOpacity}
          style={styles.spotifyTextWrapper}
          onPress={() => handleUri(cardUrl)}>
          <Image source={spotifyIcon} style={styles.spotifyIcon} />
          <Text style={styles.spotifyText}>Open Spotify</Text>
        </TouchableOpacity>
      ) : (
        <TouchableOpacity
          activeOpacity={activeOpacity}
          style={styles.spotifyTextWrapper}
          onPress={handleInstall}>
          <Image source={spotifyIcon} style={styles.spotifyIcon} />
          <Text style={styles.spotifyText}>Get Spotify Free</Text>
        </TouchableOpacity>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  cardWrapper: {
    width: '100%',
    alignItems: 'center',
    backgroundColor: theme.color.background,
    marginTop: theme.space.sm,
  },
  card: {
    // backgroundColor: theme.color.lighter,
    //borderRadius: theme.radius.xl,
    height: HEIGHT / 3.5,
    width: WIDTH - 150,
    shadowColor: theme.color.black,
    shadowOpacity: 0.3,
    shadowOffset: {width: 0, height: 0},
    elevation: 5,
    overflow: 'hidden',
  },
  image: {
    flex: 1,
  },
  textWrapper: {
    width: WIDTH / 1.5,
    marginTop: theme.space.xs,
    alignItems: 'center',
    padding: theme.space.sm,
    backgroundColor: theme.color.background,
  },
  artist: {
    color: theme.color.text,
    fontFamily: 'FuturaStd-Bold',
    fontWeight: 'bold',
    fontSize: theme.text.size.base,
    textAlign: 'center',
    letterSpacing: -0.8,
  },
  song: {
    color: theme.color.text,
    marginTop: theme.space.xs,
    textAlign: 'center',
    fontFamily: 'Roboto-Regular',
  },
  spotifyTextWrapper: {
    marginTop: theme.space.xs,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    padding: theme.space.sm,
    backgroundColor: theme.color.background,
  },
  spotifyText: {
    color: theme.color.spotify,
    textTransform: 'uppercase',
    fontFamily: 'Roboto-Regular',
    letterSpacing: 0,
  },
  spotifyIcon: {
    width: 24,
    height: 24,
    marginRight: theme.space.xs,
  },
});

export default React.memo(Card);
