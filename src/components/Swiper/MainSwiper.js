import {StyleSheet, View} from 'react-native';
import React, {useCallback, useEffect, useRef, useState} from 'react';
import Swiper from 'react-native-deck-swiper';
import RNShake from 'react-native-shake';
import {useSelector} from 'react-redux';

import sadEmo from '../../assets/sadEmo.png';
import Card from './Card';
import CardEmpty from './CardEmpty';
import {selectRecos} from '../../redux/recosSlice';
import {selectUserPremium} from '../../redux/premiumSlice';
import Loading from '../Loading';

const MainSwiper = ({index, onSwipedAllCards, onSwiped}) => {
  const [loading, setLoading] = useState(false);
  const {recos} = useSelector(selectRecos);
  const swiperRef = useRef(null);

  useEffect(() => {
    if (!recos) {
      setLoading(true);
    }
    setLoading(false);
  }, [recos]);

  useEffect(() => {
    const subscription = RNShake.addListener(() => {
      swiperRef.current.swipeRight();
    });
    return () => {
      subscription.remove();
    };
  }, [swiperRef]);

  const renderCard = useCallback(
    card => {
      if (recos?.length > 0) {
        return <Card card={card} />;
      } else {
        return (
          <View style={styles.emptyCardWrap}>
            <CardEmpty
              title="Oh bah mince !"
              text="Il n'y a aucune recommandations"
              img={sadEmo}
            />
          </View>
        );
      }
    },
    [recos],
  );

  if (loading) {
    return <Loading />;
  }

  return (
    <Swiper
      ref={swiperRef}
      containerStyle={styles.swipercontainer}
      cards={recos ? recos : []}
      backgroundColor={'transparent'}
      cardIndex={index}
      renderCard={renderCard}
      verticalSwipe={false}
      showSecondCard
      disableBottomSwipe
      disableTopSwipe
      onSwipedAll={onSwipedAllCards}
      onSwiped={card => onSwiped(card)}
      stackSize={3}
      stackScale={3}
      stackSeparation={2}
      swipeAnimationDuration={500}
      zoomFriction={20}
      zoomAnimationDuration={500}
    />
  );
};

const styles = StyleSheet.create({
  swipercontainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default MainSwiper;
