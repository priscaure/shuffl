import {View, Text, StyleSheet, Image} from 'react-native';
import React from 'react';

import {theme} from '../../theme/theme';
import {HEIGHT, WIDTH} from '../../utils/constants';

const CardEmpty = ({title, text, img}) => {
  return (
    <View style={styles.outer}>
      <View style={styles.cardWrapper}>
        <View style={styles.card}>
          <Image source={img} />
        </View>
        <View
          style={[
            styles.textWrapper,
            {
              height: title ? 70 : 0,
              backgroundColor: title ? theme.color.background : 'transparent',
            },
          ]}>
          <Text style={styles.artist}>{title}</Text>
          <Text style={styles.song}>{text}</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  outer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  cardWrapper: {
    width: '100%',
    alignItems: 'center',
    backgroundColor: theme.color.background,
  },
  card: {
    backgroundColor: theme.color.lighter,
    borderRadius: theme.radius.xl,
    height: HEIGHT / 3,
    width: WIDTH - 150,
    shadowColor: theme.color.black,
    shadowOpacity: 0.3,
    shadowOffset: {width: 0, height: 0},
    elevation: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textWrapper: {
    width: WIDTH / 1.5,
    marginTop: theme.space.sm,
    alignItems: 'center',
    paddingTop: theme.space.sm,
  },
  artist: {
    color: theme.color.text,
    fontWeight: 'bold',
    fontSize: theme.text.size.base,
  },
  song: {
    color: theme.color.text,
    marginTop: theme.space.xs,
  },
});

export default React.memo(CardEmpty);
