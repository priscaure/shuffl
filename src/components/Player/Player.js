import {View, StyleSheet, TouchableOpacity} from 'react-native';
import React, {useState, useEffect} from 'react';
import {useSelector} from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';
import SpotifyWebApi from 'spotify-web-api-node';
import RNShake from 'react-native-shake';
import {SPOTIFY_API_URL} from '@env';

import {theme} from '../../theme/theme';
import {activeOpacity, WIDTH} from '../../utils/constants';
import {selectUserToken} from '../../redux/userSlice';
import {translate} from '../../../App';
import {Notifier, Easing, NotifierComponents} from 'react-native-notifier';

const spotifyApi = new SpotifyWebApi();

const Player = ({uri, swiped, disabled}) => {
  const {token} = useSelector(selectUserToken);
  const [isPlaying, setIsPlaying] = useState(false);
  const [device, setDevice] = useState(null);

  spotifyApi.setAccessToken(token);

  useEffect(() => {
    const subscription = RNShake.addListener(() => {
      spotifyApi.getMyCurrentPlaybackState().then(data => {
        if (data.body.is_playing) {
          spotifyApi.pause();
          setIsPlaying(false);
        }
      });
    });

    return () => {
      subscription.remove();
    };
  }, []);

  useEffect(() => {
    if (token) {
      fetch(`${SPOTIFY_API_URL}/me/player/devices`, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + token,
        },
      })
        .then(res => res.json())
        .then(userDevice => setDevice(userDevice?.devices[0]?.id))
        .catch(err => console.log(err));
    }
  }, [token]);

  const togglePlay = () => {
    spotifyApi
      .getMyCurrentPlaybackState()
      .then(data => {
        if (data.body.is_playing) {
          spotifyApi.pause();
          setIsPlaying(false);
        } else {
          spotifyApi
            .play({
              uris: [uri],
              device_id: device,
              position_ms: data.body.progress_ms,
            })
            .then(() => {
              console.log('playing: ', uri);
            })
            .catch(err => {
              console.log('ERR: ', err);
            });
          setIsPlaying(true);
        }
      })
      .catch(err => {
        console.log('ERR: ', err);
        return Notifier.showNotification({
          title: 'Oups 😱',
          description: translate('toast'),
          Component: NotifierComponents.Alert,
          componentProps: {
            alertType: 'error',
          },
          duration: 5000,
          showAnimationDuration: 800,
          showEasing: Easing.ease,
          hideOnPress: true,
        });
      });
  };

  return (
    <View style={styles.player}>
      <TouchableOpacity
        activeOpacity={activeOpacity}
        onPress={togglePlay}
        disabled={disabled}>
        <View
          style={[
            styles.icon,
            {
              backgroundColor: disabled
                ? theme.color.lighter
                : theme.color.primary,
            },
          ]}>
          <Icon
            name={!isPlaying || swiped ? 'play' : 'pause'}
            size={28}
            color={theme.color.background}
          />
        </View>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  player: {
    height: 60,
    width: WIDTH,
    backgroundColor: theme.color.lighter,
    alignItems: 'center',
    justifyContent: 'center',
    borderTopLeftRadius: theme.radius.lg,
    borderTopRightRadius: theme.radius.lg,
    paddingHorizontal: theme.space.md,
    flexDirection: 'row',
  },
  icon: {
    height: 60,
    width: 60,
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: -50,
    borderColor: theme.color.background,
    shadowColor: theme.color.background,
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.7,
    shadowRadius: 9,
    elevation: 14,
  },
});

export default Player;
