import {View, Image, StyleSheet, TouchableOpacity} from 'react-native';
import React, {useEffect, useState} from 'react';
import {useSelector} from 'react-redux';
import {SPOTIFY_API_URL} from '@env';

import {selectUserToken} from '../redux/userSlice';
import robot from '../assets/robot.png';
import {theme} from '../theme/theme';
import {activeOpacity} from '../utils/constants';
import {useNavigation} from '@react-navigation/native';

const HeaderFace = () => {
  const navigation = useNavigation();
  const {token} = useSelector(selectUserToken);
  const [profileImg, setProfileImg] = useState(null);

  useEffect(() => {
    if (token) {
      fetch(`${SPOTIFY_API_URL}/me`, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + token,
        },
      })
        .then(res => res.json())
        .then(me => setProfileImg(me.images[0].url))
        .catch(err => console.log('ERROR ME: ', err));
    }
  }, [token]);

  return (
    <TouchableOpacity
      style={styles.headerFace}
      onPress={() => navigation.navigate('Profil')}
      activeOpacity={activeOpacity}>
      {!profileImg ? (
        <Image source={robot} />
      ) : (
        <Image
          source={{uri: profileImg}}
          resizeMode="cover"
          style={styles.profileImg}
        />
      )}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  headerFace: {
    backgroundColor: theme.color.primary,
    height: 30,
    width: 30,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
  },
  profileImg: {
    flex: 1,
    height: 30,
    width: 30,
  },
});

export default React.memo(HeaderFace);
