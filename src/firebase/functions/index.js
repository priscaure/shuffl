const functions = require("firebase-functions");
const {google} = require("googleapis");
const axios = require("axios");

exports.validateIOS = functions.region("europe-west2").https.onCall(
    async (d) => {
      const data = JSON.stringify({
        "receipt-data": d["receipt-data"],
        "password": d.password,
        "exclude-old-transactions": true,
      });
      const result = await axios.post("https://buy.itunes.apple.com/verifyReceipt", data);
      functions.logger.info(result, {structuredData: true});
      if (result.data.status == 21007) {
        // Sandbox
        const resultSand = await axios.post("https://sandbox.itunes.apple.com/verifyReceipt", data);
        const receiptData = resultSand.data.latest_receipt_info[0];
        const expirationDate = receiptData.expires_date_ms;
        const expired = Date.now() > expirationDate;

        return {
          isExpired: expired,
        };
      } else {
        // Prod
        const receiptData = result.data.latest_receipt_info[0];
        const expirationDate = receiptData.expires_date_ms;
        const expired = Date.now() > expirationDate;

        return {
          isExpired: expired,
        };
      }
    });

exports.validateAndroid = functions.region("europe-west2").https.onCall(
    async (data) => {
      functions.logger.info(data, {structuredData: true});
      const auth = new google.auth.GoogleAuth({
        keyFile: "pc-api-9150211663299200430-64-81792058e327.json",
        scopes: ["https://www.googleapis.com/auth/androidpublisher"],
      });
      functions.logger.info(JSON.parse(data).purchaseToken, {
        structuredData: true,
      });

      try {
        const res = await google
            .androidpublisher("v3")
            .purchases.subscriptions.get({
              packageName: "com.priscaure.shufflapp",
              subscriptionId: JSON.parse(data).productId,
              token: JSON.parse(data).purchaseToken,
              auth: auth,
            });
        functions.logger.info(res, {
          structuredData: true,
        });
        if (res.status === 200) {
          functions.logger.info(res.data.paymentState === 1, {
            structuredData: true,
          });
          return {isActiveSubscription: res.data.paymentState === 1};
        }
        return {error: -1};
      } catch (error) {
        functions.logger.error(error, {
          structuredData: true,
        });
        return new functions.https.HttpsError(error.code, error.details);
      }
    });
