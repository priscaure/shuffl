import {Image} from 'react-native';
import React, {useEffect, useState} from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {SPOTIFY_API_URL} from '@env';

import AuthScreen from '../screens/AuthScreen';
import HomeScreen from '../screens/HomeScreen';
import SettingScreen from '../screens/SettingScreen';
import {theme} from '../theme/theme';
import HeaderFace from '../components/HeaderFace';
import headerLogo from '../assets/headerLogo.png';
import {useDispatch, useSelector} from 'react-redux';
import {selectUserToken, setToken, setUserMe} from '../redux/userSlice';
import Menu from '../components/Menu';
import InfoScreen from '../screens/InfoScreen';
import AsyncStorage from '@react-native-async-storage/async-storage';
import PremiumScreen from '../screens/PremiumScreen';
import Loading from '../components/Loading';
import useInAppPurchase from '../hooks/useInAppPurchase';
import {Notifier, NotifierComponents, Easing} from 'react-native-notifier';
import AboutScreen from '../screens/AboutScreen';
import {getUserId, refreshToken} from '../utils/helpers';
import OneSignal from 'react-native-onesignal';
import GeneralScreen from '../screens/GeneralScreen';
import ProfilScreen from '../screens/ProfilScreen';

const Stack = createNativeStackNavigator();

const options = {
  headerTitle: () => <Image source={headerLogo} />,
  headerRight: () => <HeaderFace />,
  headerTitleAlign: 'center',
  headerBackTitle: '',
  headerTintColor: theme.color.primary,
  headerStyle: {
    backgroundColor: theme.color.background,
  },
};

const MainNavigator = () => {
  const {connectionErrorMsg} = useInAppPurchase();
  const dispatch = useDispatch();
  const {token} = useSelector(selectUserToken);
  const now = Math.floor(Date.now() / 1000);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const getUserData = async () => {
      const user = await getUserId(token);
      let externalUserId = user.id;
      OneSignal.setExternalUserId(externalUserId, results => {
        // The results will contain push and email success statuses
        console.log('Results of setting external user id');
        console.log(results);

        // Push can be expected in almost every situation with a success status, but
        // as a pre-caution its good to verify it exists
        if (results.push && results.push.success) {
          console.log('Results of setting external user id push status:');
          console.log(results.push.success);
        }
      });
    };
    getUserData();
  }, [token]);

  useEffect(() => {
    if (token) {
      fetch(`${SPOTIFY_API_URL}/me`, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + token,
        },
      })
        .then(res => res.json())
        .then(me => {
          dispatch(setUserMe(me));
        })
        .catch(err => console.log('ERROR ME: ', err));
    }
  }, [token, dispatch]);

  useEffect(() => {
    if (connectionErrorMsg) {
      return Notifier.showNotification({
        title: 'Oups 😱',
        description: connectionErrorMsg,
        Component: NotifierComponents.Alert,
        componentProps: {
          alertType: 'error',
        },
        duration: 5000,
        showAnimationDuration: 800,
        showEasing: Easing.ease,
        hideOnPress: true,
      });
    }
  }, [connectionErrorMsg]);

  useEffect(() => {
    setLoading(true);
    const getRefreshToken = async () => {
      const hasExpirationTime = await AsyncStorage.getItem('@tokenExp');
      const expirationTime = JSON.parse(hasExpirationTime);
      if (token) {
        console.log('NOW: ', now);
        console.log('expirationTime: ', expirationTime);
        if (now > expirationTime) {
          const refreshData = await refreshToken();
          console.log('refreshData:', refreshData);

          if (refreshData) {
            dispatch(setToken(refreshData.access_token));
            AsyncStorage.setItem(
              '@refreshToken',
              JSON.stringify(refreshData.refresh_token),
            );
            AsyncStorage.setItem(
              '@tokenExp',
              JSON.stringify(Math.floor(Date.now() / 1000)),
            );
            setLoading(false);
          } else {
            dispatch(setToken(null));
            setLoading(false);
          }
          setLoading(false);
        }
      } else {
        console.log('NO TOKEN');
        dispatch(setToken(null));
        setLoading(false);
      }
    };
    getRefreshToken();
    setLoading(false);
  }, [now, token, dispatch]);

  if (loading) {
    return <Loading />;
  }

  return (
    <Stack.Navigator screenOptions={{animation: 'slide_from_right'}}>
      {!token ? (
        <Stack.Screen
          name="Auth"
          component={AuthScreen}
          options={{headerShown: false}}
        />
      ) : (
        <>
          <Stack.Screen
            name="Info"
            component={InfoScreen}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="Home"
            component={HomeScreen}
            options={{
              headerStyle: {
                backgroundColor: theme.color.background,
              },
              headerBackVisible: false,
              headerLeft: () => <Menu />,
              headerTitle: () => <Image source={headerLogo} />,
              headerTitleAlign: 'center',
              headerRight: () => <HeaderFace />,
            }}
          />
          <Stack.Screen
            name="Profil"
            component={ProfilScreen}
            options={options}
          />
          <Stack.Screen
            name="Settings"
            component={SettingScreen}
            options={options}
          />
          <Stack.Screen
            name="General"
            component={GeneralScreen}
            options={options}
          />
          <Stack.Screen
            name="About"
            component={AboutScreen}
            options={options}
          />
          <Stack.Screen
            name="Premium"
            component={PremiumScreen}
            options={options}
          />
        </>
      )}
    </Stack.Navigator>
  );
};

export default MainNavigator;
