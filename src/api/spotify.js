import {CLIENT_ID, CLIENT_SECRET} from '@env';

export const config = {
  clientId: CLIENT_ID,
  clientSecret: CLIENT_SECRET,
  redirectUrl: 'shufflapp:/oauthredirect',
  scopes: [
    'user-read-currently-playing',
    'user-read-recently-played',
    'user-read-playback-state',
    'user-modify-playback-state',
    'user-read-email',
    'user-read-private',
    'user-follow-read',
    'playlist-read-collaborative',
    'playlist-read-private',
    'user-read-private',
    'user-library-read',
    'user-top-read',
    'streaming',
  ],
  serviceConfiguration: {
    authorizationEndpoint: 'https://accounts.spotify.com/authorize',
    tokenEndpoint: 'https://accounts.spotify.com/api/token',
  },
};
