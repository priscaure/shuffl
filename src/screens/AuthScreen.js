import {SafeAreaView, StyleSheet, StatusBar} from 'react-native';
import React from 'react';

import {theme} from '../theme/theme';
import AuthButton from '../components/buttons/AuthButton';
import AuthBackground from '../components/auth/AuthBackground';
import AuthText from '../components/auth/AuthText';
import AuthLogo from '../components/auth/AuthLogo';
import {translate} from '../../App';

const AuthScreen = () => {
  return (
    <AuthBackground>
      <SafeAreaView style={styles.container}>
        <StatusBar
          backgroundColor={theme.color.background}
          barStyle="light-content"
        />
        <AuthLogo />
        <AuthText first={translate('text1')} second={translate('text2')} />
        <AuthButton text={translate('login')} />
      </SafeAreaView>
    </AuthBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: theme.space.xxl,
  },
});

export default AuthScreen;
