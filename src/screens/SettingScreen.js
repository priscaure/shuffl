import {
  SafeAreaView,
  View,
  StyleSheet,
  StatusBar,
  Alert,
  Linking,
  Text,
} from 'react-native';
import React, {useCallback} from 'react';
import {PRIVACY_URL, TERMS_URL} from '@env';

import {theme} from '../theme/theme';
import AuthLogo from '../components/auth/AuthLogo';
import SettingsTitle from '../components/SettingsTitle';
import SettingsRow from '../components/Settings/SettingsRow';
import {translate} from '../../App';
// import PremiumButton from '../components/Premium/PremiumButton';
// import {useSelector} from 'react-redux';
// import {selectUserPremium} from '../redux/premiumSlice';

const SettingScreen = ({navigation}) => {
  // const {premium} = useSelector(selectUserPremium);
  const handlePress = useCallback(async url => {
    const supported = await Linking.canOpenURL(url);

    if (supported) {
      await Linking.openURL(url);
    } else {
      Alert.alert(`Don't know how to open this URL: ${url}`);
    }
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar
        backgroundColor={theme.color.background}
        barStyle="light-content"
      />
      <View>
        <View style={styles.img}>
          <AuthLogo />
          {/* <Text style={styles.premium}>{premium ? 'Premium' : 'Free'}</Text> */}
        </View>
        {/* <View style={styles.btnWrap}>
          <PremiumButton />
        </View> */}
        <View style={{padding: theme.space.md}}>
          <SettingsTitle title={translate('settings')} />
          <SettingsRow
            iconName="sliders"
            text={translate('general')}
            onPress={() => navigation.navigate('General')}
            chevron
          />
          <SettingsRow
            iconName="smile"
            text={translate('about')}
            chevron
            onPress={() => navigation.navigate('About')}
          />
          {/* <SettingsRow
            iconName="star"
            text={translate('premiumExp')}
            onPress={() => navigation.navigate('Premium')}
            chevron
          /> */}
          <View style={styles.spacer}>
            <SettingsTitle title={translate('links')} />
          </View>
          <SettingsRow
            iconName="eye"
            text={translate('privacy')}
            onPress={() => handlePress(PRIVACY_URL)}
            chevron
          />
          <SettingsRow
            iconName="link"
            text={translate('terms')}
            onPress={() => handlePress(TERMS_URL)}
            chevron
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.color.background,
    justifyContent: 'space-between',
  },
  img: {
    alignItems: 'center',
    padding: theme.space.lg,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: theme.color.border,
  },
  premium: {
    color: theme.color.primary,
    fontSize: theme.text.size.md,
    fontFamily: 'FuturaStd-Medium',
  },
  btnWrap: {
    alignItems: 'center',
    paddingHorizontal: theme.space.lg,
    paddingBottom: theme.space.md,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: theme.color.border,
  },
  spacer: {
    marginTop: theme.space.md,
  },
});

export default SettingScreen;
