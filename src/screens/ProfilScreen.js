import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  StatusBar,
  Platform,
} from 'react-native';
import React from 'react';
import {useSelector} from 'react-redux';

// import {selectUserPremium} from '../redux/premiumSlice';
import {theme} from '../theme/theme';
import AuthLogo from '../components/auth/AuthLogo';
import {selectUserToken} from '../redux/userSlice';
import LogoutButton from '../components/buttons/LogoutButton';

const ProfilScreen = () => {
  const {userMe} = useSelector(selectUserToken);
  // const {premium} = useSelector(selectUserPremium);

  return (
    <SafeAreaView style={styles.container}>
      <>
        <StatusBar
          backgroundColor={theme.color.background}
          barStyle="light-content"
        />
        <View>
          <View style={styles.img}>
            <AuthLogo />
            {/* <Text style={styles.premium}>{premium ? 'Premium' : 'Free'}</Text> */}
          </View>
          <View style={{padding: theme.space.md}}>
            <View style={styles.row}>
              <Text style={styles.name}>{userMe?.display_name}</Text>
              <Text style={styles.text}>Spotify: {userMe?.product}</Text>
            </View>
            <View style={styles.spacer} />
            <View style={styles.row}>
              <Text style={styles.title}>Country</Text>
              <Text style={styles.text}>{userMe?.country}</Text>
            </View>
            <View style={styles.spacer} />
            <View style={styles.row}>
              <Text style={styles.title}>Email</Text>
              <Text style={styles.text}>{userMe?.email}</Text>
            </View>
          </View>
        </View>
      </>
      <View style={styles.logout}>
        <LogoutButton />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.color.background,
    justifyContent: 'space-between',
  },
  img: {
    alignItems: 'center',
    padding: theme.space.lg,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: theme.color.border,
  },
  premium: {
    color: theme.color.primary,
    fontSize: theme.text.size.md,
    fontFamily: 'FuturaStd-Medium',
  },
  row: {
    alignItems: 'center',
    marginBottom: theme.space.md,
  },
  name: {
    color: theme.color.primary,
    fontSize: theme.text.size.xxl,
    flexWrap: 'wrap',
    fontFamily: 'FuturaStd-Bold',
    marginBottom: theme.space.xs,
    letterSpacing: -0.8,
  },
  title: {
    color: theme.color.primary,
    fontSize: theme.text.size.lg,
    flexWrap: 'wrap',
    fontFamily: 'FuturaStd-Bold',
    marginBottom: theme.space.xs,
    letterSpacing: -0.8,
  },
  text: {
    color: theme.color.text,
    fontFamily: 'Roboto-Regular',
  },
  spacer: {
    height: StyleSheet.hairlineWidth,
    backgroundColor: theme.color.lighter,
    marginBottom: theme.space.md,
  },
  round: {
    height: 32,
    width: 32,
    backgroundColor: theme.color.primary,
    borderRadius: 16,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logout: {
    paddingVertical: theme.space.md,
  },
});

export default ProfilScreen;
