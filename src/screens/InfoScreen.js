import {
  Animated,
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  Image,
  TouchableOpacity,
  StatusBar,
} from 'react-native';
import React, {useEffect, useRef} from 'react';
import RNShake from 'react-native-shake';

import shake from '../assets/shake.png';
import shakeRobot from '../assets/shakeRobot.png';
import wiggle from '../assets/wiggle.png';
import {theme} from '../theme/theme';
import {translate} from '../../App';

const InfoScreen = ({navigation}) => {
  const shakeAnim = useRef(new Animated.Value(0)).current;

  const shakeRobotAnim = () => {
    Animated.loop(
      Animated.sequence([
        Animated.timing(shakeAnim, {
          toValue: 10,
          duration: 100,
          useNativeDriver: true,
        }),
        Animated.timing(shakeAnim, {
          toValue: -10,
          duration: 100,
          useNativeDriver: true,
        }),
        Animated.timing(shakeAnim, {
          toValue: 10,
          duration: 100,
          useNativeDriver: true,
        }),
        Animated.timing(shakeAnim, {
          toValue: 0,
          duration: 100,
          useNativeDriver: true,
        }),
      ]),
    ).start(() => shakeRobotAnim());
  };

  useEffect(() => {
    shakeRobotAnim();
  }, []);

  useEffect(() => {
    const subscription = RNShake.addListener(() => {
      navigation.navigate('Home');
    });

    return () => {
      subscription.remove();
    };
  }, [navigation]);

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar
        backgroundColor={theme.color.background}
        barStyle="light-content"
      />
      <View style={styles.wrap}>
        <Image source={shake} />
        <Text style={styles.subText}>{translate('shufflePhone')}</Text>
        <View style={styles.innerWrap}>
          <Image source={wiggle} style={styles.wiggle} />
          <Animated.View style={{transform: [{translateX: shakeAnim}]}}>
            <Image source={shakeRobot} />
          </Animated.View>
          <View style={styles.callout}>
            <Text style={styles.calloutText}>{translate('callout')}</Text>
          </View>
          <TouchableOpacity onPress={() => navigation.navigate('Home')}>
            <Text style={styles.shakeText}>{translate('nowShake')}</Text>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: theme.space.md,
    backgroundColor: theme.color.background,
    alignItems: 'center',
  },
  wrap: {
    paddingHorizontal: theme.space.lg,
    paddingVertical: theme.space.xl,
  },
  subText: {
    fontFamily: 'FuturaStd-Medium',
    color: theme.color.text,
    fontSize: theme.text.size.xl,
    textAlign: 'center',
    paddingTop: theme.space.sm,
    paddingBottom: theme.space.md,
  },
  innerWrap: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'space-between',
    paddingVertical: theme.space.xl,
  },
  callout: {
    padding: theme.space.sm,
    borderWidth: StyleSheet.hairlineWidth,
    borderColor: theme.color.text,
    borderRadius: theme.radius.base,
    width: '100%',
  },
  calloutText: {
    color: theme.color.text,
    fontSize: theme.text.size.base,
    textAlign: 'center',
    lineHeight: 20,
  },
  shakeText: {
    fontFamily: 'FuturaStd-Medium',
    color: theme.color.primary,
    fontSize: theme.text.size.xl,
    textAlign: 'center',
    paddingTop: theme.space.sm,
  },
  wiggle: {
    position: 'absolute',
    top: '30%',
    zIndex: 10,
  },
});

export default InfoScreen;
