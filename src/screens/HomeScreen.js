import {SafeAreaView, Image, View, StyleSheet, StatusBar} from 'react-native';
import React, {useState, useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import SpotifyWebApi from 'spotify-web-api-node';
import {Notifier, Easing, NotifierComponents} from 'react-native-notifier';
import {SPOTIFY_API_URL} from '@env';

import {theme} from '../theme/theme';
import {selectUserToken} from '../redux/userSlice';
import discover from '../assets/discover.png';
import {WIDTH} from '../utils/constants';
import Player from '../components/Player/Player';
import CardEmpty from '../components/Swiper/CardEmpty';
import wiggle from '../assets/wiggle.png';
import confetti from '../assets/confetti.png';
import MainSwiper from '../components/Swiper/MainSwiper';
import {selectRecos, setRecos} from '../redux/recosSlice';
import {translate} from '../../App';
import {selectSwiperIndex, setSwiperIndex} from '../redux/swiperSlice';
import {selectUserPremium} from '../redux/premiumSlice';
import OneSignal from 'react-native-onesignal';
import {
  fetchArtists,
  fetchFeaturedPlaylists,
  fetchPlaylist,
  fetchTracks,
} from '../utils/helpers';
import Loading from '../components/Loading';
// import BottomSheetContent from '../components/BottomSheetContent';

const spotifyApi = new SpotifyWebApi();
const LIMIT = 100;

const HomeScreen = () => {
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();
  const {token, userMe} = useSelector(selectUserToken);
  const {recos} = useSelector(selectRecos);
  const {swiperIndex, lessThanOneDay} = useSelector(selectSwiperIndex);
  const {premium, expired} = useSelector(selectUserPremium);
  const [limit, setLimit] = useState(LIMIT);
  const [artistsArr, setArtistsArr] = useState([]);
  const [tracksArr, setTracksArr] = useState([]);
  const [playlistArr, setPlaylistArr] = useState([]);
  const seedArtists = artistsArr.join();
  const seedtracks = tracksArr.join();
  const [isAllSwiped, setIsAllSwiped] = useState(false);
  const [swiped, setSwiped] = useState(false);

  spotifyApi.setAccessToken(token);
  console.log('TOKEN: ', token);

  useEffect(() => {
    setLoading(true);
    if (artistsArr.length === 0 && tracksArr.length === 0) {
      const getFeaturedData = async () => {
        const playlistID = await fetchFeaturedPlaylists(token);

        const data = {
          token: token,
          id: playlistID,
        };

        const playlistData = await fetchPlaylist(data);
        setPlaylistArr();
        dispatch(setRecos(playlistData.tracks.items));
      };
      getFeaturedData();
    }
    setLoading(false);
  }, [token, artistsArr, tracksArr, dispatch]);

  useEffect(() => {
    OneSignal.sendTag('premium', JSON.stringify(premium));
  }, [premium]);

  useEffect(() => {
    setLoading(true);
    const fetchArtistsData = async () => {
      if (token && artistsArr.length === 0) {
        const artists = await fetchArtists(token);
        if (artists) {
          artists?.items.map(artist => {
            setArtistsArr(oldArr => [...oldArr, artist.id]);
            setLoading(false);
          });
        }
      }
    };
    fetchArtistsData();
    setLoading(false);
  }, [token, artistsArr]);

  useEffect(() => {
    setLoading(true);
    const fetchTracksData = async () => {
      if (token && tracksArr.length === 0) {
        const topTracks = await fetchTracks(token);
        if (topTracks) {
          topTracks?.items.map(topTrack => {
            setTracksArr(oldArr => [...oldArr, topTrack.id]);
            setLoading(false);
          });
        }
      }
    };
    fetchTracksData();
    setLoading(false);
  }, [token, tracksArr]);

  useEffect(() => {
    setLoading(true);
    const fetchRecosData = async () => {
      if (token) {
        await fetch(
          `${SPOTIFY_API_URL}/recommendations?seed_artists=${seedArtists}&seed_tracks=${seedtracks}&limit=${limit}`,
          {
            method: 'GET',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
              Authorization: 'Bearer ' + token,
            },
          },
        )
          .then(res => res.json())
          .then(recoRes => {
            dispatch(setRecos(recoRes.tracks));
            setLoading(false);
          })
          .catch(err => {
            console.log(err);
            setLoading(false);
          });
      }
    };
    fetchRecosData();
    setLoading(false);
  }, [token, seedArtists, seedtracks, dispatch, limit]);

  const onSwipedAllCards = () => {
    setIsAllSwiped(true);
  };

  const onSwiped = () => {
    if (swiperIndex === 99) {
      setIsAllSwiped(true);
      dispatch(setSwiperIndex(0));
    } else {
      dispatch(setSwiperIndex(swiperIndex + 1));
    }

    spotifyApi
      .getMyCurrentPlaybackState()
      .then(data => {
        if (data.body.is_playing) {
          spotifyApi.pause();
          setSwiped(true);
        }
      })
      .catch(err => {
        console.log('ERR: ', err);
        if (userMe?.product === 'premium') {
          return Notifier.showNotification({
            title: 'Oups 😱',
            description: translate('toast'),
            Component: NotifierComponents.Alert,
            componentProps: {
              alertType: 'error',
            },
            duration: 5000,
            showAnimationDuration: 800,
            showEasing: Easing.bounce,
            hideOnPress: true,
          });
        }
      });
  };

  if (loading) {
    return <Loading />;
  }

  return (
    <SafeAreaView style={styles.container}>
      <StatusBar
        backgroundColor={theme.color.background}
        barStyle="light-content"
      />
      <Image
        source={discover}
        style={{width: WIDTH, marginTop: theme.space.xl}}
        resizeMode="contain"
      />
      <View style={styles.swiperWrap}>
        <Image source={wiggle} style={styles.wiggle} />
        {!isAllSwiped ? (
          <MainSwiper
            onSwiped={onSwiped}
            onSwipedAllCards={onSwipedAllCards}
            //index={swiperIndex}
          />
        ) : (
          <CardEmpty
            title={translate('emptyCardTitle')}
            text={translate('emptyCardText')}
            img={confetti}
          />
        )}
      </View>
      {userMe?.product === 'premium' && recos && (
        <Player
          uri={
            recos
              ? recos[swiperIndex]?.uri
                ? recos[swiperIndex]?.uri
                : recos[swiperIndex]?.track.uri
              : null
          }
          swiped={swiped}
          disabled={isAllSwiped}
        />
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.color.background,
  },
  emptyCardWrap: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  swiperWrap: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: theme.color.background,
  },
  swipercontainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  bg: {
    backgroundColor: theme.color.lighter,
    shadowColor: theme.color.black,
    shadowOpacity: 0.5,
    shadowOffset: {width: 0, height: 9},
    shadowRadius: 12.35,
    elevation: 19,
  },
  wiggle: {
    width: '80%',
    position: 'absolute',
    top: '40%',
    zIndex: 10,
  },
});

export default HomeScreen;
