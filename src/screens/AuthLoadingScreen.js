import {View, Text} from 'react-native';
import React, {useEffect, useState} from 'react';
import {CLIENT_ID, CLIENT_SECRET} from '@env';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useDispatch, useSelector} from 'react-redux';
import {selectUserToken, setToken} from '../redux/userSlice';
import Loading from '../components/Loading';

const AuthLoadingScreen = () => {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const now = new Date().getTime();
  const {token} = useSelector(selectUserToken);

  useEffect(() => {
    const getRefreshToken = async () => {
      const hasRefreshToken = await AsyncStorage.getItem('@refreshToken');
      const hasExpirationTime = await AsyncStorage.getItem('@tokenExp');
      const parsedRefreshToken = JSON.parse(hasRefreshToken);
      const expirationTime = new Date(JSON.parse(hasExpirationTime)).getTime();
      const myHeaders = new Headers();
      myHeaders.append('Content-Type', 'application/x-www-form-urlencoded');

      const requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: `grant_type=refresh_token&refresh_token=${parsedRefreshToken}&client_id=${CLIENT_ID}&client_secret=${CLIENT_SECRET}`,
      };

      if (token) {
        if (now > expirationTime) {
          console.log('TOKEN EXPIRED');
          setLoading(true);
          await fetch('https://accounts.spotify.com/api/token', requestOptions)
            .then(response => response.text())
            .then(res => {
              dispatch(setToken(res.accessToken));
              setLoading(false);
            })
            .catch(error => {
              console.log('error', error);
              dispatch(setToken(null));
              setLoading(false);
            });
        }
      } else {
        console.log('NO TOKEN');
        dispatch(setToken(null));
        setLoading(false);
      }
    };
    getRefreshToken();
  }, [token, now, dispatch]);

  return <Loading />;
};

export default AuthLoadingScreen;
