import {Image, Text, View, SafeAreaView, StyleSheet} from 'react-native';
import React from 'react';

import {theme} from '../theme/theme';
import premiumImg from '../assets/premiumImg.png';
import SettingsTitle from '../components/SettingsTitle';
import SettingsRow from '../components/Settings/SettingsRow';
import LinearGradient from 'react-native-linear-gradient';
import {HEIGHT} from '../utils/constants';
import {translate} from '../../App';

const PremiumScreen = () => {
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.premiumImg}>
        <Image source={premiumImg} />
      </View>
      <View style={{padding: theme.space.md}}>
        <SettingsTitle title="Current" />
        <SettingsRow iconName="check-square" text={translate('premiumTask1')} />
        <View style={styles.divider} />
        <LinearGradient
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
          colors={[theme.color.primary, theme.color.secondary]}
          style={styles.spacer}>
          <Text style={styles.more}>{translate('premiumSoon')}</Text>
          <Text style={styles.subtitle}>{translate('premiumTuned')}</Text>
        </LinearGradient>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.color.background,
  },
  premiumImg: {
    marginVertical: theme.space.xl,
    alignItems: 'center',
  },
  spacer: {
    marginTop: theme.space.md,
    paddingTop: theme.space.md,
    paddingHorizontal: theme.space.md,
    paddingBottom: 0,
    height: HEIGHT / 3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  divider: {
    marginTop: theme.space.md,
    marginBottom: theme.space.md,
    borderTopWidth: StyleSheet.hairlineWidth,
    borderTopColor: theme.color.border,
  },
  more: {
    color: theme.color.background,
    fontFamily: 'FuturaStd-Bold',
    fontSize: 28,
    lineHeight: 32,
    marginBottom: theme.space.md,
    letterSpacing: -1,
    textAlign: 'center',
  },
  subtitle: {
    color: theme.color.background,
    fontSize: theme.text.size.md,
  },
});

export default PremiumScreen;
