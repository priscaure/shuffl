import {View, Text, SafeAreaView, StyleSheet, Switch} from 'react-native';
import React, {useState, useEffect} from 'react';
import OneSignal from 'react-native-onesignal';

import {theme} from '../theme/theme';
import SettingsTitle from '../components/SettingsTitle';
import {Notifier, NotifierComponents, Easing} from 'react-native-notifier';
import {translate} from '../../App';

const GeneralScreen = () => {
  const [isEnabled, setIsEnabled] = useState(false);

  useEffect(() => {
    const getUSerDeviceState = async () => {
      const device = await OneSignal.getDeviceState();
      console.log('device: ', device.isSubscribed);
      setIsEnabled(device.isSubscribed);
    };
    getUSerDeviceState();
  }, [isEnabled]);

  const disableNotif = () => {
    OneSignal.disablePush(true);
    setIsEnabled(false);
    Notifier.showNotification({
      title: '🔕',
      description: translate('notificationsDisable'),
      Component: NotifierComponents.Alert,
      componentProps: {
        alertType: 'error',
      },
      duration: 5000,
      showAnimationDuration: 800,
      showEasing: Easing.ease,
      hideOnPress: true,
    });
  };
  const enableNotif = () => {
    OneSignal.disablePush(false);
    setIsEnabled(true);
    Notifier.showNotification({
      title: '🔔',
      description: translate('notificationsEnable'),
      Component: NotifierComponents.Alert,
      componentProps: {
        alertType: 'success',
      },
      duration: 5000,
      showAnimationDuration: 800,
      showEasing: Easing.ease,
      hideOnPress: true,
    });
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={{padding: theme.space.md}}>
        <SettingsTitle title="Notifications" />
        <View style={styles.spacer} />
        <View style={styles.row}>
          <Text style={styles.text}>{translate('notificationsLabel')}</Text>
          <Switch
            trackColor={{true: theme.color.primary, false: theme.color.lighter}}
            thumbColor={isEnabled ? theme.color.text : theme.color.text}
            ios_backgroundColor={
              !isEnabled ? theme.color.lighter : theme.color.primary
            }
            onValueChange={isEnabled ? disableNotif : enableNotif}
            value={isEnabled}
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.color.background,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: theme.space.lg,
  },
  text: {
    color: theme.color.text,
    fontFamily: 'Roboto-Regular',
  },
  spacer: {
    height: StyleSheet.hairlineWidth,
    backgroundColor: theme.color.lighter,
    marginBottom: theme.space.md,
  },
});
export default GeneralScreen;
