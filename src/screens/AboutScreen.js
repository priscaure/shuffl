import {SafeAreaView, View, Text, StyleSheet} from 'react-native';
import React from 'react';
import {theme} from '../theme/theme';
import SettingsTitle from '../components/SettingsTitle';

const AboutScreen = () => {
  return (
    <SafeAreaView style={styles.container}>
      <View style={{padding: theme.space.md}}>
        <SettingsTitle title="Général" />
        <View style={styles.spacer} />
        <View style={styles.row}>
          <Text style={styles.text}>Version</Text>
          <Text style={styles.text}>1.0.3</Text>
        </View>
        <SettingsTitle title="Crédits" />
        <View style={styles.spacer} />
        <View style={styles.row}>
          <Text style={styles.text}>Prisca A.</Text>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: theme.color.background,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: theme.space.lg,
  },
  text: {
    color: theme.color.text,
    fontFamily: 'Roboto-Regular',
  },
  spacer: {
    height: StyleSheet.hairlineWidth,
    backgroundColor: theme.color.lighter,
    marginBottom: theme.space.md,
  },
});

export default AboutScreen;
