import {createSlice} from '@reduxjs/toolkit';

const premiumSlice = createSlice({
  name: 'premium',
  initialState: {
    premium: false,
    expired: false,
    products: {},
  },
  reducers: {
    setPremium: (state, action) => {
      state.premium = action.payload;
    },
    setExpired: (state, action) => {
      state.expired = action.payload;
    },
    setProducts: (state, action) => {
      state.products = action.payload;
    },
  },
});

export const {setPremium, setExpired, setProducts} = premiumSlice.actions;
// selector
export const selectUserPremium = state => state.premium;

export default premiumSlice.reducer;
