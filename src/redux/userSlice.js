import {createSlice} from '@reduxjs/toolkit';

const userSlice = createSlice({
  name: 'user',
  initialState: {
    token: null,
    notificationsSubscribed: false,
    userMe: {},
  },
  reducers: {
    setToken: (state, action) => {
      state.token = action.payload;
    },
    setNotificationsSubscribed: (state, action) => {
      state.token = action.payload;
    },
    setUserMe: (state, action) => {
      state.userMe = action.payload;
    },
  },
});

export const {setToken, setNotificationsSubscribed, setUserMe} =
  userSlice.actions;
// selector
export const selectUserToken = state => state.user;

export default userSlice.reducer;
