import {createSlice} from '@reduxjs/toolkit';

const recosSlice = createSlice({
  name: 'recos',
  initialState: {
    recos: [],
  },
  reducers: {
    setRecos: (state, action) => {
      state.recos = action.payload;
    },
  },
});

export const {setRecos} = recosSlice.actions;
// selector
export const selectRecos = state => state.recos;

export default recosSlice.reducer;
