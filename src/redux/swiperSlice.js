import {createSlice} from '@reduxjs/toolkit';

const swiperSlice = createSlice({
  name: 'swiperIndex',
  initialState: {
    swiperIndex: 0,
    lessThanOneDay: false,
  },
  reducers: {
    setSwiperIndex: (state, action) => {
      state.swiperIndex = action.payload;
    },
    setLessThanOneDay: (state, action) => {
      state.lessThanOneDay = action.payload;
    },
  },
});

export const {setSwiperIndex, setLessThanOneDay} = swiperSlice.actions;
// selector
export const selectSwiperIndex = state => state.swiperIndex;

export default swiperSlice.reducer;
